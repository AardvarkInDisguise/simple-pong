#ifndef STANDARD_FUNCTIONS
#define STANDARD_FUNCTIONS

#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS
#endif

#ifndef GL_UNSIGNED_SHORT_5_6_5
#define GL_UNSIGNED_SHORT_5_6_5 0x8363
#endif
#ifndef GL_CLAMP_TO_EDGE
#define GL_CLAMP_TO_EDGE 0x812F
#endif

#define SCREEN_WIDTH 720
#define SCREEN_HEIGHT 480
#define SCREEN_BPP 32
#define SCREEN_FPS 60

#define PI 3.14159265

#include "SDL.h"
#include "SDL_error.h"
#include "SDL_image.h"
#include "SDL_ttf.h"
#include "SDL_mixer.h"
#include <string>
#include <sstream>
#include <assert.h>
#include <list>
#include <vector>
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <Windows.h>
#include "gl/gl.h"
#include "gl/glu.h"
#include "gl/glext.h"
#include <math.h>
#include <ctime>
#include <cstdlib>
#include "FRect.h"

char* SDL_GetError(void);

namespace TS
{
	//blit a surface to another surface with sprite clipping, returns false on error
	void ApplySurface(int x, int y, SDL_Surface* source, SDL_Surface* destination, SDL_Rect *clip);

	//blit a surface to another surface without sprite clipping, returns false on error
	void ApplySurface(int x, int y, SDL_Surface* source, SDL_Surface* destination);

	//load an image from file as a surface, returns NULL on error
	SDL_Surface* LoadImage(std::string filename);

	//get a random number between nLow and nHigh
	int GetRandomNumber(int nLow, int nHigh);

	//check for collision between two objects
	bool CheckRectRectCollision(FRect A, FRect B);

	bool CheckCircRectCollision(float circX, float circY, float circR, FRect rect);

	//gets the direction from one location to another
	double GetDirection(int x1, int y1, int x2, int y2);

	//returns a new unique id
	unsigned long int GetUniqueID();

	double GetDistance( float x1, float y1, float x2, float y2 );
}

#endif