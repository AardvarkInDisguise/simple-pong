#ifndef ENTITY_H
#define ENTITY_H

#include "standard_functions.h"
#include "Texture.h"
#include "timer.h"
#include "Collision.h"
#include "FPoint.h"

class Entity
{
	protected:
		//bounding box
		FRect m_frecBBox;
		//true if the gamestate should delete this entity
		bool m_bShouldDelete;
		
		float m_fXVel;
		float m_fYVel;

		//track time between frames
		Timer m_tmrMotion;

		std::vector<Collision> m_vecCollisions;

		//each entity should have a unique ID
		unsigned long int m_nID;

	public:
		virtual void HandleEvents(SDL_Event event) = 0;
		virtual void DoMovement() = 0;
		virtual void DoCollision() = 0;
		virtual void DoLogic() = 0;
		virtual void Render() = 0;

		virtual ~Entity(){};

		void MarkForDelete(){m_bShouldDelete = true;};
		bool GetShouldDelete(){return m_bShouldDelete;};

		FRect GetBBox(){return m_frecBBox;};
		int GetXVel(){return m_fXVel;};
		int GetYVel(){return m_fYVel;};

		unsigned int GetID(){return m_nID;};
};

#endif