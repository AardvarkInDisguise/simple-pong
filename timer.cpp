#include "timer.h"

//initialize the variables
Timer::Timer()
{
	//clock time when the timer started
	startTicks = 0;
	//clock time when the timer was paused
	pausedTicks = 0;

	//is the timer paused
	bPaused = false;

	//has the timer started (both this and bPaused can be true the difference is bStarted is true
	//if the timer has been started bPaused is true if the timer has been started but isn't counting right now)
	bStarted = false;
}

//starts the timer, if the timer is already running then it essentially resets it
void Timer::Start()
{
	bStarted = true;
	bPaused = false;

	startTicks = SDL_GetTicks();
}

//stop the timer and reset values
void Timer::Stop()
{
	bStarted = false;
	bPaused = false;

	startTicks = 0;
}

//get the timers current time, if the timer isn't running returns -1
int Timer::GetTime()
{
	//if the timer is actually running
	if(bStarted == true)
	{
		//if the timer is paused then return the time the timer was paused at
		if(bPaused == true)
		{
			return pausedTicks;
		}
		//if the timer isn't paused then return the timers current time
		else
		{
			//time is calculated by subtracting the clock time of when the timer started from the current clock time
			return SDL_GetTicks() - startTicks;
		}
	}
	//return -1 if the timer isn't running
	return -1;
}

//set the timer to a specified time
void Timer::SetTime(int time)
{
	if(bStarted == true)
	{
		startTicks = SDL_GetTicks() - time;
	}
}

//pause the timer
void Timer::Pause()
{
	//make sure the timer is already running and isn't already paused
	if((bStarted == true) && (bPaused == false))
	{
		bPaused = true;
		//store the current timer time in paused ticks
		pausedTicks = SDL_GetTicks() - startTicks;
	}
}

//unpause the timer
void Timer::UnPause()
{
	//if the timer was actually paused (we don't need to check if it's started because bPaused is only true if bStarted is true)
	if(bPaused == true)
	{
		bPaused = false;

		//get the current clock time then subtract the time when the timer was paused
		//the startTicks will then be behind the clock time by the amount on the timer when it was paused
		startTicks = SDL_GetTicks() - pausedTicks;

		//reset pausedTicks just to be clean
		pausedTicks = 0;
	}
}

//get if the timer has been started
bool Timer::IsStarted()
{
	return bStarted;
}
//get if the timer is currently paused
bool Timer::IsPaused()
{
	return bPaused;
}