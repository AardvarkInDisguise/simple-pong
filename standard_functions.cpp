#include "standard_functions.h"

//load an image from file as a surface
SDL_Surface *TS::LoadImage(std::string filename)
{
	//temp storage for the loaded file
	SDL_Surface *loadedImage = NULL;
	//storage for the image after it has been optimized (this is returned)
	SDL_Surface *optimizedImage = NULL;

	//load the image
	loadedImage = IMG_Load(filename.c_str());
	if(loadedImage != NULL)
	{
		//convert the image to the screens diaplay format
		optimizedImage = SDL_DisplayFormatAlpha(loadedImage);

		SDL_FreeSurface(loadedImage);
	}

	return optimizedImage;
}

//blit a surface to another surface without sprite clipping
void TS::ApplySurface(int x, int y, SDL_Surface* source, SDL_Surface* destination)
{
	//store the x, y in a Rect because SDL_BlitSurface() only takes Rects for the offset
	SDL_Rect offset;
	offset.x = x;
	offset.y = y;

	//blit the surface, if everything was ok returns true else it returns false
	if(SDL_BlitSurface(source, NULL, destination, &offset) != 0)
	{
		printf("SDL_BlitSurface failed: %s\n", SDL_GetError());
	}
}

//blit a surface to another surface with sprite clipping
void TS::ApplySurface(int x, int y, SDL_Surface* source, SDL_Surface* destination, SDL_Rect *clip = NULL)
{
	//store the x, y in a Rect because SDL_BlitSurface() only takes Rects for the offset
	SDL_Rect offset;
	offset.x = x;
	offset.y = y;

	//blit the surface, if everything was ok returns true else it returns false
	if(SDL_BlitSurface(source, clip, destination, &offset) != 0)
	{
		printf("SDL_BlitSurface failed: %s\n", SDL_GetError());
	}
}

//get a random number between nLow and nHigh
int TS::GetRandomNumber(int nLow, int nHigh)
{
	int low = nLow;
	int high = nHigh;
	if(nLow < 0)
	{
		low += -nLow;
		high += -nLow;
	}
    int num = ((rand() % (high - low + 1)) + low);

	if(nLow < 0)
	{
		num +=nLow;
	}

	return num;
}

//check for collision between two rectangular objects
bool TS::CheckRectRectCollision(FRect A, FRect B)
{
	//variables to hold info on sides of rectangles
	float leftA,   leftB,
		  rightA,  rightB,
		  topA,	   topB,
		  bottomA, bottomB;

	//store the sides of A in the variables
	leftA = A.x;
	rightA = A.x + A.w;
	topA = A.y;
	bottomA = A.y + A.h;

	//store the sides of B in the variables
	leftB = B.x;
	rightB = B.x + B.w;
	topB = B.y;
	bottomB = B.y + B.h;

	//if any of the sides from A are outside of B
	if(bottomA <= topB)
		return false;
	if(topA >= bottomB)
		return false;
	if(rightA <= leftB)
		return false;
	if(leftA >= rightB)
		return false;

	return true;
}

bool TS::CheckCircRectCollision(float circX, float circY, float circR, FRect rect)
{
	float refX, refY;

	if(circX < rect.x)
	{
		refX = rect.x;
	}
	else if(circX > rect.x + rect.w)
	{
		refX = rect.x + rect.w;
	}
	else
	{
		refX = circX;
	}

	if(circY < rect.y)
	{
		refY = rect.y;
	}
	else if(circY > rect.y + rect.h)
	{
		refY = rect.y + rect.h;
	}
	else
	{
		refY = circY;
	}

	if(TS::GetDistance(circX, circY, refX, refY) < circR)
	{
		return true;
	}
	else return false;
}

double TS::GetDirection(int x1, int y1, int x2, int y2)
{
	float a = x2-x1;
	float b = y2-y1;

	if(a == 0)
	{
		if(y2 < y1)
			return 90;
		else if(y2 > y1)
			return 270;
		else
			return 0;
	}

	double angle = atan(b/a) * 180 / PI;

	//angle will be between -90 and 90 here so we need to add to it accordingly
	if(a >= 0 && b<0)
	{
		angle += 180;
	}
	else if(a > 0 && b > 0)
	{
		angle += 180;
	}
	else if(a < 0 && b >= 0)	
	{
		angle += 360;
	}

	return angle;
}

unsigned long int TS::GetUniqueID()
{
	static unsigned long int nID = 0;
	return nID++;
}

double TS::GetDistance(float x1, float y1, float x2, float y2)
{
    //Return the distance between the two points
    return sqrt(pow((double)x2 - (double)x1, 2) + pow((double)y2 - (double)y1, 2));
}