#ifndef TEXTURE_H
#define TEXTURE_H

#include "standard_functions.h"
#include "FRect.h"

class Texture
{
	private:
		GLuint m_nTextureID;
		GLuint m_nTextureWidth;
		GLuint m_nTextureHeight;
		GLuint* m_pnPixels;

	public:
		Texture();
		~Texture();

		bool LoadTextureFromPixels32(GLvoid* pnPixels, GLuint nWidth, GLuint nHeight);
		bool LoadTextureFromFile(std::string strFile);
		bool LoadTextureFromSDLSurface(SDL_Surface* srf);

		void FreeTexture();

		void Render(GLfloat fX, GLfloat fY);
		void Render(GLfloat fX, GLfloat fY, FRect* recClip, bool bOverrideColor = false);
		void Render(GLfloat fX, GLfloat fY, bool bOverrideColor);

		bool Lock();
		bool UnLock();

		GLuint* GetPixelData32();
		GLuint  GetPixel32(GLuint x, GLuint y);
		void    SetPixel32(GLuint x, GLuint y, GLuint nPixel);
		
		GLuint GetTextureID(){return m_nTextureID;};
		GLuint GetTextureWidth(){return m_nTextureWidth;};
		GLuint GetTextureHeight(){return m_nTextureHeight;};
};

#endif