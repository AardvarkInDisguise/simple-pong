#include "standard_functions.h"
#include "GameState.h"
#include "GameStateTitle.h"
#include "GameStatePlaying.h"
#include "timer.h"
#include "Button.h"

SDL_Surface* srfScreen;

void Init()
{
	//Start SDL
	SDL_Init( SDL_INIT_EVERYTHING );
	srfScreen = SDL_SetVideoMode(SCREEN_WIDTH, SCREEN_HEIGHT, SCREEN_BPP, SDL_SWSURFACE | SDL_OPENGL);

	TTF_Init();

	//get some info about the system and print it to the screen
	std::cout << "GL Version:   " << glGetString(GL_VERSION) << std::endl;
	std::cout << "GL Vendor:    " << glGetString(GL_VENDOR) << std::endl;
	std::cout << "GL Renderer:  " << glGetString(GL_RENDERER) << std::endl;
	std::cout << "GLSL Version: " <<glGetString(GL_SHADING_LANGUAGE_VERSION) << std::endl <<std::endl;

	//set resolution
	glViewport(0.0, 0.0, SCREEN_WIDTH, SCREEN_HEIGHT);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0.0, SCREEN_WIDTH, SCREEN_HEIGHT, 0.0, 1.0, -1.0);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glPushMatrix();

	glClearColor(0.0, 0.0, 0.0, 1.0);

	glEnable(GL_TEXTURE_2D);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);

	GLenum error = glGetError();
	if(error != GL_NO_ERROR)
	{
		std::cout << "ERROR Failed to initialise OpenGL!" << std::endl << gluErrorString(error);
		system("Pause");
		exit(1);
	}

	srand(std::time(0));
}

int main(int argc, char* args[])
{
	Init();

	//timer used to track and limit the fps
	Timer tmrFPS;
	bool bQuit = false;
	SDL_Event event;

	//start on the title screen
	GameState* gameState = new GameStateTitle();

	while(!bQuit)
	{
		tmrFPS.Start();

		while(SDL_PollEvent(&event))
		{
			gameState->HandleEvents(event);
			switch(event.type)
			{
				case SDL_QUIT:
					bQuit = true;
					break;
			}
		}

		gameState->Dologic();

		gameState->DoRender();

		SDL_GL_SwapBuffers();
		glClear(GL_COLOR_BUFFER_BIT);

		//handle gamestate change if the next state is defined
		if(gameState->GetNextStateID() != GAMESTATE_NULL)
		{
			switch(gameState->GetNextStateID())
			{
				case GAMESTATE_EXIT:
					delete gameState;
					bQuit = true;
					std::cout << "GameState = Exit" << std::endl;
					break;
				case GAMESTATE_TITLE:
					delete gameState;
					gameState = new GameStateTitle();
					break;
				case GAMESTATE_PLAY:
					delete gameState;
					gameState = new GameStatePlaying();
					break;
				default:
					std::cerr << "ERROR: MAIN FUNCTION. Unhandled Game State Change, Not Changing" << std::endl;
					break;
			}
		}

		//delay next loop to maintain fps limit
		if((tmrFPS.GetTime() < 1000 / SCREEN_FPS))
		{
			SDL_Delay((1000/SCREEN_FPS) - tmrFPS.GetTime());
			
		}
	}

	//Quit SDL 
	SDL_Quit();
	TTF_Quit();

	system("Pause");
	return 0;
}