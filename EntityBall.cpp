#include "EntityBall.h"

EntityBall::EntityBall(int x, int y, int diameter, std::vector<Entity*> *entVec, int nInitSpeed, int *anPlayerScores, bool bDynSpd, bool bEdgeKills)
{
	m_nID = TS::GetUniqueID();

	m_frecBBox.x = x;
	m_frecBBox.y = y;
	m_frecBBox.h = diameter;
	m_frecBBox.w = diameter;

	m_vecEnts = entVec;

	m_bShouldDelete = false;

	m_nSpeed = nInitSpeed;
	m_fDir = 0;

	float fDir = 0;
	//get a random direction
	fDir = TS::GetRandomNumber(-50, 50);
	SetDir(fDir);

	if(TS::GetRandomNumber(0, 1) == 1)
		FlipXVel();
	
	m_bDynamicSpeed = bDynSpd;
	m_bEdgeKills = bEdgeKills;

	m_anPlayerScores = anPlayerScores;

	m_tmrMotion.Start();
}

EntityBall::~EntityBall()
{
	//there is no dynamically allocated memory
	std::cout << "EB DELETED" << std::endl;
}

void EntityBall::SetDir(float fDir)
{
	m_fDir = fDir;
	while(m_fDir < 360)
		m_fDir += 360;
	while(m_fDir >= 360)
		m_fDir -= 360;

	m_fXVel = cos(m_fDir*PI/180.0) * m_nSpeed;
	m_fYVel = sin(m_fDir*PI/180.0) * m_nSpeed;
}

void EntityBall::SetSpeed(int nSpeed)
{
	m_nSpeed = nSpeed;

	m_fXVel = cos(m_fDir*PI/180.0) * m_nSpeed;
	m_fYVel = sin(m_fDir*PI/180.0) * m_nSpeed;
}

void EntityBall::HandleEvents(SDL_Event event)
{
	//this entity is physically simulated and doesn't use events
}

void EntityBall::DoMovement()
{
	//standard time based movement
	m_frecBBox.x += (m_fXVel * ((float)m_tmrMotion.GetTime()/1000.f));
	m_frecBBox.y += (m_fYVel * ((float)m_tmrMotion.GetTime()/1000.f));

	m_tmrMotion.Start();
}

void EntityBall::DoCollision()
{
	for(std::vector<Entity*>::iterator iii = m_vecEnts->begin(); iii != m_vecEnts->end(); iii++)
	{
		//if the entity isn't this entity or a ball then check for collision
		if((*iii)->GetID() != m_nID && typeid(*(*iii)) != typeid(EntityBall))
		{
			if(TS::CheckCircRectCollision(m_frecBBox.x + m_frecBBox.w/2, m_frecBBox.y + m_frecBBox.h/2, m_frecBBox.h/2, (*iii)->GetBBox()))
			{
				Collision col;
				col.type = COLLISION_ENTITY;
				col.BBox = (*iii)->GetBBox();
				col.typeName = typeid(*(*iii)).name();
				m_vecCollisions.push_back(col);
			}
		}
	}

	//if the ball hit any edge of the screen
	if(m_frecBBox.x < 0 || m_frecBBox.x + m_frecBBox.w > SCREEN_WIDTH)
	{
		Collision col;
		col.type = COLLISION_SCREENDOMAIN;
		m_vecCollisions.push_back(col);
	}
	if(m_frecBBox.y < 0 || m_frecBBox.y + m_frecBBox.h > SCREEN_HEIGHT)
	{
		Collision col;
		col.type = COLLISION_SCREENRANGE;
		m_vecCollisions.push_back(col);
	}
}

void EntityBall::DoLogic()
{
	bool bBrickHit = false;
	for(std::vector<Collision>::iterator iii = m_vecCollisions.begin(); iii != m_vecCollisions.end(); iii++)
	{
		switch(iii->type)
		{
			//if the ball went off the top of bottom of the screen
			case COLLISION_SCREENRANGE:
				if(m_frecBBox.y < 0)
				{
					m_frecBBox.y = 0;
				}
				else if((m_frecBBox.y + m_frecBBox.h) > SCREEN_HEIGHT)
				{
					m_frecBBox.y = SCREEN_HEIGHT - m_frecBBox.h;
				}
				FlipYVel();
				break;
			//if the ball went fully off the left of right side of the screen
			case COLLISION_SCREENDOMAIN:
				if(m_bEdgeKills)
				{
					if(m_frecBBox.x < -(m_frecBBox.w))
					{
						MarkForDelete();
						if(m_anPlayerScores != NULL)
						{
							m_anPlayerScores[0]++;
						}
					}
					else if(m_frecBBox.x > SCREEN_WIDTH)
					{
						MarkForDelete();
						if(m_anPlayerScores != NULL)
						{
							m_anPlayerScores[1]++;
						}
					}
				}
				else
				{
					if(m_frecBBox.x < 0)
					{
						m_frecBBox.x = 0;
					}
					else if((m_frecBBox.x + m_frecBBox.w) > SCREEN_HEIGHT)
					{
						m_frecBBox.x = SCREEN_WIDTH - m_frecBBox.w;
					}
					FlipXVel();
				}
				break;
			//if the ball hit an entity
			case COLLISION_ENTITY:
				//if the ball hit a paddle then change angle of movement based on where the ball hit
				//otherwise just reverse direction
				if(iii->typeName == typeid(EntityPaddle).name())
				{
					//flip direction if the ball hits the paddle, angle of reflection is dependent on where the ball hits the paddle
					if(m_fXVel < 0)
					{
						SetDir(60 * (((m_frecBBox.y + m_frecBBox.h/2) - (iii->BBox.y)) - (iii->BBox.h/2))/(iii->BBox.h/2));
						m_frecBBox.x = iii->BBox.x + iii->BBox.w;
						if(m_bDynamicSpeed)
							SetSpeed(m_nSpeed + 5);
					}
					else
					{
						SetDir(60 * (((m_frecBBox.y + m_frecBBox.h/2) - (iii->BBox.y)) - (iii->BBox.h/2))/(iii->BBox.h/2));
						m_frecBBox.x = iii->BBox.x - m_frecBBox.w;
						FlipXVel();
						if(m_bDynamicSpeed)
							SetSpeed(m_nSpeed + 5);
					}
				}
				break;

			default:
				std::cerr << "ERROR: Entity Ball, unhandled collision type" << std::endl;
				break;
		}
		m_vecCollisions.erase(iii--);
	}
}

void EntityBall::Render()
{
	//render an orange-red circle
	glLoadIdentity();
	glColor3f(1.0, 0.3, 0.0);

	glTranslatef(m_frecBBox.x, m_frecBBox.y, 0.0);
	glTranslatef(m_frecBBox.w/2, m_frecBBox.h/2, 0.0);

	glBegin(GL_TRIANGLE_FAN);
		glVertex2f(0, 0);
		for(float iii = 0.0; iii <= 360; iii += 360/15)
			glVertex2f(cos(iii*PI/180.0) * m_frecBBox.w/2, sin(iii*PI/180.0) * m_frecBBox.h/2);
		
	glEnd();
	
}

//calculates which side the ball hit
//the far left side is #1 and it moves around clockwise,
//each edge and corner gets a number so #2 is the top left
//corner and #3 is the top edge
int EntityBall::GetCollisionSide(FRect frecOtherEnt)
{
	FPoint refX, refY;
	float circX, circY;
	circX = m_frecBBox.x + m_frecBBox.w/2;
	circY = m_frecBBox.y + m_frecBBox.h/2;

	//get a reference point for x on the box edge
	if(circX < frecOtherEnt.x)
	{
		refX.x = frecOtherEnt.x;
	}
	else if(circX > frecOtherEnt.x + frecOtherEnt.w)
	{
		refX.x = frecOtherEnt.x + frecOtherEnt.w;
	}
	else
	{
		refX.x = circX;
	}

	//set the Y position of the x reference to either the top or bottom of the rect depending on which side the ball is on
	if(circY > (frecOtherEnt.y + frecOtherEnt.h/2))
	{
		refX.y = frecOtherEnt.y + frecOtherEnt.h;
	}
	else
	{
		refX.y = frecOtherEnt.y;
	}

	//get a reference point for y on the box edge
	if(circY < frecOtherEnt.y)
	{
		refY.y = frecOtherEnt.y;
	}
	else if(circY > frecOtherEnt.y + frecOtherEnt.h)
	{
		refY.y = frecOtherEnt.y + frecOtherEnt.h;
	}
	else
	{
		refY.y = circY;
	}

	//set the X position of the y reference point to the left or right side of the box depending on which side the ball is on
	if(circX > (frecOtherEnt.x + frecOtherEnt.w/2))
	{
		refY.x = frecOtherEnt.x + frecOtherEnt.w;
	}
	else
	{
		refY.x = frecOtherEnt.x;
	}

	//if the ball is closer to the X reference then it hit the top or bottom of the box
	if(TS::GetDistance(circX, circY, refX.x, refX.y) < TS::GetDistance(circX, circY, refY.x, refY.y))
	{
		if(circY > (frecOtherEnt.y + frecOtherEnt.h/2))
		{
			return 7;
		}
		else
		{
			return  3;
		}
	}
	//otherwise it hit the left or right side
	else
	{
		if(circX > (frecOtherEnt.x + frecOtherEnt.w/2))
		{
			return 5;
		}
		else
		{
			return 1;
		}
	}

	//if we haven't returned by now then the ball is an equal distance to both references
	//that means it hit a corner
	if(circY > (frecOtherEnt.y + frecOtherEnt.h/2))
	{
		if(circX > (frecOtherEnt.x + frecOtherEnt.w/2))
		{
			return 6;
		}
		else if(circX < (frecOtherEnt.x + frecOtherEnt.w/2))
		{
			return 8;
		}
	}
	else if(circY < (frecOtherEnt.y + frecOtherEnt.h/2))
	{
		if(circX > (frecOtherEnt.x + frecOtherEnt.w/2))
		{
			return 4;
		}
		else if(circX < (frecOtherEnt.x + frecOtherEnt.w/2))
		{
			return 2;
		}
	}

	//if something somehow went horribly wrong then return -1 but I don't see how this could ever be reached
	return -1;
}

inline void EntityBall::FlipXVel()
{
	SetDir((-m_fDir) + 180);
}

inline void EntityBall::FlipYVel()
{
	SetDir(-m_fDir);
}