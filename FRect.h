#ifndef FRECT_H
#define FRECT_H

#include "standard_functions.h"

struct FRect
{
	float x;
	float y;
	float w;
	float h;
};

#endif