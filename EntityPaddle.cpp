#include "EntityPaddle.h"

EntityPaddle::EntityPaddle(int x, int y, int w, int h, std::vector<Entity*> *entVec, bool bEnableAI)
{
	m_nID = TS::GetUniqueID();

	m_vecEnts = entVec;

	m_nMaxSpeed = 200;

	m_nHitPos = 0;

	m_frecBBox.h = h;
	m_frecBBox.w = w;
	m_frecBBox.x = x;
	m_frecBBox.y = y;

	m_bShouldDelete = false;

	//start with the paddle not moving
	m_nMoveState = MOVESTATE_STOP;

	//start the timers
	m_tmrMotion.Start();

	m_bAIEnabled = bEnableAI;

	m_tmrBallLogic.Start();
	m_tmrBallLogic.SetTime(2000);
	m_tmrMoveLogic.Start();
}

EntityPaddle::~EntityPaddle()
{
	//there is no dynamically allocated memory
	std::cout << "EP DELETED" << std::endl;
}

void EntityPaddle::HandleEvents(SDL_Event event)
{
	//if the AI isn't running then accept player input
	if(!m_bAIEnabled)
	{
		m_fYVel = 0;

		Uint8* pnKeys = SDL_GetKeyState(NULL);
		if(!(pnKeys[SDLK_UP] && pnKeys[SDLK_DOWN]))
		{
			if(pnKeys[SDLK_UP])
			{
				m_fYVel = -m_nMaxSpeed;
			}
			else if(pnKeys[SDLK_DOWN])
			{
				m_fYVel = m_nMaxSpeed;
			}
		}
	}
}

void EntityPaddle::DoMovement()
{
	//standard time based movement
	m_frecBBox.x += (m_fXVel * ((float)m_tmrMotion.GetTime()/1000.f));
	m_frecBBox.y += (m_fYVel * ((float)m_tmrMotion.GetTime()/1000.f));

	m_tmrMotion.Start();
}

void EntityPaddle::DoCollision()
{
	//if the paddle went off the top or bottom of the screen
	if(m_frecBBox.y < 0 || (m_frecBBox.y + m_frecBBox.h) > SCREEN_HEIGHT)
	{
		Collision col;
		col.type = COLLISION_SCREENRANGE;
		m_vecCollisions.push_back(col);
	}
}

void EntityPaddle::DoLogic()
{
	for(std::vector<Collision>::iterator iii = m_vecCollisions.begin(); iii != m_vecCollisions.end(); iii++)
	{
		switch(iii->type)
		{
			//if the paddle went off the screen put it back on screen touching the edge
			case COLLISION_SCREENRANGE:
				if(m_frecBBox.y < 0)
				{
					m_frecBBox.y = 0;
				}
				else if((m_frecBBox.y + m_frecBBox.h) > SCREEN_HEIGHT)
				{
					m_frecBBox.y = SCREEN_HEIGHT - m_frecBBox.h;
				}
				//m_fYVel = -m_fYVel;
				break;

			default:
				std::cerr << "ERROR: Entity Paddle, unhandled collision type" << std::endl;
				break;
		}
		m_vecCollisions.erase(iii--);
	}

	if(m_bAIEnabled)
		DoAI();
}

void EntityPaddle::Render()
{
	//draw a white rectangle where the paddle is
	glLoadIdentity();
	glColor3f(1.0, 1.0, 1.0);

	glTranslatef(m_frecBBox.x, m_frecBBox.y, 0.0);

	glBegin(GL_QUADS);
		glVertex2f(0.0, 0.0);
		glVertex2f(0.0, m_frecBBox.h);
		glVertex2f(m_frecBBox.w, m_frecBBox.h);
		glVertex2f(m_frecBBox.w, 0.0);
	glEnd();

	glLoadIdentity();
	glColor3f(1.0, 0.0, 0.0);

	glTranslatef(m_frecBBox.x + m_frecBBox.w, m_nHitPos, 0.0);

	glBegin(GL_QUADS);
		glVertex2f(0.0, 0.0);
		glVertex2f(5.0, 0.0);
		glVertex2f(5.0, 5.0);
		glVertex2f(0.0, 5.0);
	glEnd();
}

void EntityPaddle::DoAI()
{
	//vars to hold info about the ball
	int nBallX = -1;
	int nBallY = -1;
	int nBallD = -1;
	int nBallXV = -1;
	int nBallYV = -1;

	for(std::vector<Entity*>::iterator iii = m_vecEnts->begin(); iii != m_vecEnts->end(); iii++)
	{
		//if we found the ball then get the info and exit the loop
		if(typeid((*(*iii))) == typeid(EntityBall))
		{
			nBallX = (*iii)->GetBBox().x;
			nBallY = (*iii)->GetBBox().y;
			nBallD = (*iii)->GetBBox().h;
			nBallXV = (*iii)->GetXVel();
			nBallYV = (*iii)->GetYVel();

			break;
		}
	}
	//if the ball info was found
	if(nBallX != -1)
	{
		//if the paddle can keep up with the ball
		if(abs(nBallYV) < m_nMaxSpeed)
		{
			//if the ball is lower than the paddle move the paddle down and vice-versa or stop the paddle if it lines up with the ball
			if((nBallY + nBallD/2) < (m_frecBBox.y + m_frecBBox.h/4))
			{
				m_nMoveState = MOVESTATE_UP;
			}
			else if((nBallY + nBallD/2) > (m_frecBBox.y + m_frecBBox.h/4))
			{
				m_nMoveState = MOVESTATE_DOWN;
			}
			else
			{
				m_nMoveState = MOVESTATE_STOP;
			}
		}
		//if the paddle can't keep up with the ball then use a predictive method
		else
		{
			//update prediction every tenth of a second
			if(m_tmrBallLogic.GetTime() > 100)
			{
				m_tmrBallLogic.Start();

				float fTimeToHit = 0;

				//if the ball is moving to the right the time untill it hits the paddle is the distance to the far edge(relative to paddle) plus
				//the width of the screen all divided by the x velocity of the ball
				if(m_frecBBox.x < SCREEN_WIDTH/2)
				{
					if(nBallXV > 0)
					{
						int nEdge = SCREEN_WIDTH;
					
						fTimeToHit = (((float)nEdge - ((float)nBallX + (float)nBallD * 2.0)) + ((float)nEdge - (m_frecBBox.x + m_frecBBox.w)))/(float)nBallXV;
					}
					//otherwise the time is the the distance from the near edge (relative to paddle) divided by the ball's x velocity
					else if(nBallXV < 0)
					{
						fTimeToHit = -((float)nBallX - (m_frecBBox.x + m_frecBBox.w))/((float)nBallXV);
					}
				}
				else
				{
					if(nBallXV < 0)
					{
						//fTimeToHit = (((float)nBallX) + ((float)SCREEN_WIDTH))/((float)nBallXV);
						fTimeToHit = ((-((float)nBallX + (float)nBallD * 2.0)) + (-(m_frecBBox.x + m_frecBBox.w)))/(float)nBallXV;
					}
					//otherwise the time is the the distance from the near edge (relative to paddle) divided by the ball's x velocity
					else if(nBallXV > 0)
					{
						fTimeToHit = ((m_frecBBox.x - m_frecBBox.w) - (float)nBallX )/((float)nBallXV);
					}
				}

				//the hit pos is the verticle distance the ball will travel before it hits the near edge(relative tto paddle)
				m_nHitPos = (float)nBallY + (float)nBallYV * fTimeToHit;
				//if the hit pos would be off the screen simulate the bounce of the ball
				while (m_nHitPos > SCREEN_HEIGHT || m_nHitPos < 0)
				{
					if(m_nHitPos < 0)
					{
						m_nHitPos = (-m_nHitPos);
					}
					else if(m_nHitPos > SCREEN_HEIGHT)
					{
						m_nHitPos = SCREEN_HEIGHT - (m_nHitPos + (nBallD * 2.0) - SCREEN_HEIGHT);
					}
				}

				m_nHitPos += nBallD/2;
			}

			//if the hit pos is lower than the paddle move the paddle down and vice-versa or stop the paddle if it lines up with the hit pos
			if(m_nHitPos <= (m_frecBBox.y + m_frecBBox.h/4))
			{
				m_nMoveState = MOVESTATE_UP;
			}
			else if(m_nHitPos >= (m_frecBBox.y + m_frecBBox.h - m_frecBBox.h/4))
			{
				m_nMoveState = MOVESTATE_DOWN;
			}
			else
			{
				m_nMoveState = MOVESTATE_STOP;
			}
		}
	}

	//check how the paddle should be moving every tenth of a second
	if(m_tmrMoveLogic.GetTime() > 50)
	{
		m_tmrMoveLogic.Start();
		switch(m_nMoveState)
		{
			case MOVESTATE_UP:
				m_fYVel = -m_nMaxSpeed;
				break;
			case MOVESTATE_DOWN:
				m_fYVel = m_nMaxSpeed;
				break;
			case MOVESTATE_STOP:
				m_fYVel = 0;
				break;
		}
	}
}

bool EntityPaddle::GetAIEnabled()
{
	return m_bAIEnabled;
}

void EntityPaddle::SetAIEnabled(bool bEnableAI)
{
	m_bAIEnabled = bEnableAI;
}