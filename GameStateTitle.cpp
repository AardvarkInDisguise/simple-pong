#include "GameStateTitle.h"

GameStateTitle::GameStateTitle()
	:m_btnPlay(SCREEN_WIDTH/2 - 100, 70, 200, 50, "Play", 0),
	m_btnQuit(SCREEN_WIDTH/2 - 100, 130, 200, 50, "Quit", 1)
{
	std::cout << "GameState = Title" << std::endl;

	stateID = GAMESTATE_TITLE;
	nextStateID = GAMESTATE_NULL;

	//generate the image for the title texture
	TTF_Font *font = TTF_OpenFont("fonts/ERASLGHT.TTF", 60);
	assert(font != NULL);
	SDL_Color textColor;
	textColor.r = 255;
	textColor.g = 255;
	textColor.b = 255;
	SDL_Surface* titleText = TTF_RenderText_Blended(font, "Pong",textColor);
	assert(titleText != NULL);
	//set the texture to the generated image
	m_texTitle.LoadTextureFromSDLSurface(titleText);

	SDL_FreeSurface(titleText);
	TTF_CloseFont(font);

	m_vecEnts.push_back(new EntityPaddle(10, 200, 10, 100, &m_vecEnts, true));
	m_vecEnts.push_back(new EntityBall(350, 300, 30, &m_vecEnts, 300, NULL, false, false));
}

GameStateTitle::~GameStateTitle()
{
	for(std::vector<Entity*>::iterator iii = m_vecEnts.begin(); iii != m_vecEnts.end(); iii++)
	{
		delete((*iii));
		std::cout << "ENT DELETE" << std::endl;
		m_vecEnts.erase(iii--);
	}

	m_texTitle.FreeTexture();

	std::cout << "GS DELETE" << std::endl;
}

void GameStateTitle::HandleEvents(SDL_Event &event)
{
	m_btnPlay.HandleEvents(event);
	m_btnQuit.HandleEvents(event);

	for(std::vector<Entity*>::iterator iii = m_vecEnts.begin(); iii != m_vecEnts.end(); iii++)
	{
		(*iii)->HandleEvents(event);
	}

	if(event.type == SDL_KEYDOWN)
	{
		if(event.key.keysym.sym == SDLK_ESCAPE)
		{
			nextStateID = GAMESTATE_EXIT;
		}
	}
}

void GameStateTitle::Dologic()
{
	m_btnPlay.DoLogic();
	m_btnQuit.DoLogic();

	//delete entities marked for deletion
	for(std::vector<Entity*>::iterator iii = m_vecEnts.begin(); iii != m_vecEnts.end(); iii++)
	{
		if((*iii)->GetShouldDelete())
		{
			delete(*iii);
			std::cout << "ENT DELETE" << std::endl;
			m_vecEnts.erase(iii--);
		}
	}

	//do movement, collision and logic for each entity
	for(std::vector<Entity*>::iterator iii = m_vecEnts.begin(); iii != m_vecEnts.end(); iii++)
	{
		(*iii)->DoMovement();
	}
	for(std::vector<Entity*>::iterator iii = m_vecEnts.begin(); iii != m_vecEnts.end(); iii++)
	{
		(*iii)->DoCollision();
	}
	for(std::vector<Entity*>::iterator iii = m_vecEnts.begin(); iii != m_vecEnts.end(); iii++)
	{
		(*iii)->DoLogic();
	}

	//handle state change via buttons
	if(m_btnQuit.GetWasClicked())
	{
		nextStateID = GAMESTATE_EXIT;
	}
	else if(m_btnPlay.GetWasClicked())
	{
		nextStateID = GAMESTATE_PLAY;
	}
}

void GameStateTitle::DoRender()
{
	//render all entities
	for(std::vector<Entity*>::iterator iii = m_vecEnts.begin(); iii != m_vecEnts.end(); iii++)
	{
		(*iii)->Render();
	}

	m_btnPlay.Render();
	m_btnQuit.Render();

	m_texTitle.Render(SCREEN_WIDTH/2 - m_texTitle.GetTextureWidth()/2, 0);
}