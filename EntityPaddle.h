#ifndef ENTITYPADDLE_H
#define ENTITYPADDLE_H

#include "Entity.h"
#include "EntityBall.h"

class EntityPaddle : public Entity
{
	protected:

		//used by AI to tell the paddle how to move
		enum MovementStates
		{
			MOVESTATE_UP,
			MOVESTATE_STOP,
			MOVESTATE_DOWN
		};
		int m_nMoveState;

		//max speed the paddle can move at
		int m_nMaxSpeed;

		std::vector<Entity*> *m_vecEnts;

		int m_nHitPos;

		int m_nBrickHitPos;

		//tracks when to update the movement state and ball location prediction
		Timer m_tmrBallLogic;
		Timer m_tmrMoveLogic;

		bool m_bAIEnabled;

		void DoAI();

	public:
		EntityPaddle(int x, int y, int w, int h, std::vector<Entity*> *entVec, bool bEnableAI);
		virtual void HandleEvents(SDL_Event event);
		virtual void DoMovement();
		virtual void DoCollision();
		virtual void DoLogic();
		virtual void Render();
		virtual ~EntityPaddle();

		bool GetAIEnabled();
		void SetAIEnabled(bool bEnableAI);
};

#endif