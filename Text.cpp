#include "Text.h"

Text::Text(std::string strText, std::string strFont, int nSize, short int nR, short int nG, short int nB, int nX, int nY)
{
	m_colTextColor.b = nB;
	m_colTextColor.g = nG;
	m_colTextColor.r = nR;

	m_strFontName = strFont;
	m_strText = strText;
	m_nFontSize = nSize;

	m_nX = nX;
	m_nY = nY;

	m_fntFont = TTF_OpenFont(m_strFontName.c_str(), m_nFontSize);
	assert(m_fntFont != NULL);
	SDL_Color textColor;
	textColor.r = 255;
	textColor.g = 255;
	textColor.b = 255;
	SDL_Surface* srfText = TTF_RenderText_Blended(m_fntFont, m_strText.c_str(), textColor);
	assert(srfText != NULL);
	//set the texture to the generated image
	m_texTextTexture.LoadTextureFromSDLSurface(srfText);
	m_nH = srfText->h;
	m_nW = srfText->w;
	SDL_FreeSurface(srfText);
	srfText = NULL;
}

Text::Text(std::string strText, std::string strFont, int nSize, SDL_Color colColor, int nX, int nY)
{
	m_strFontName = strFont;
	m_strText = strText;
	m_nFontSize = nSize;
	m_colTextColor = colColor;

	m_nX = nX;
	m_nY = nY;

	m_fntFont = TTF_OpenFont(m_strFontName.c_str(), m_nFontSize);
	assert(m_fntFont != NULL);
	SDL_Color textColor;
	textColor.r = 255;
	textColor.g = 255;
	textColor.b = 255;
	SDL_Surface* srfText = TTF_RenderText_Blended(m_fntFont, m_strText.c_str(), textColor);
	assert(srfText != NULL);
	//set the texture to the generated image
	m_texTextTexture.LoadTextureFromSDLSurface(srfText);
	m_nH = srfText->h;
	m_nW = srfText->w;
	SDL_FreeSurface(srfText);
	srfText = NULL;
}

Text::~Text()
{
	TTF_CloseFont(m_fntFont);
	std::cout << "TEXT DEL" << std::endl;
}

void Text::Render()
{
	glColor3f((float)m_colTextColor.r/255.f, (float)m_colTextColor.g/255.f, (float)m_colTextColor.b/255.f);
	m_texTextTexture.Render(m_nX, m_nY, true);
}

void Text::UpdateTexture(bool bReloadFont)
{

	m_texTextTexture.FreeTexture();
	if(bReloadFont)
	{
		TTF_CloseFont(m_fntFont);
		m_fntFont = TTF_OpenFont(m_strFontName.c_str(), m_nFontSize);
		assert(m_fntFont != NULL);
	}
	SDL_Color textColor;
	textColor.r = 255;
	textColor.g = 255;
	textColor.b = 255;
	SDL_Surface* srfText = TTF_RenderText_Blended(m_fntFont, m_strText.c_str(), textColor);
	assert(srfText != NULL);
	//set the texture to the generated image
	m_texTextTexture.LoadTextureFromSDLSurface(srfText);
	m_nH = srfText->h;
	m_nW = srfText->w;
	SDL_FreeSurface(srfText);
	srfText = NULL;
}

void Text::SetText(std::string strNewText)
{
	m_strText = strNewText;

	UpdateTexture(false);
}

void Text::SetFontSize(int nNewSize)
{
	m_nFontSize = nNewSize;
	UpdateTexture(true);
}

void Text::SetColor(SDL_Color colNewColor)
{
	m_colTextColor = colNewColor;
}

void Text::SetColor(short int nNewR, short int nNewG, short int nNewB)
{
	m_colTextColor.b = nNewB;
	m_colTextColor.r = nNewR;
	m_colTextColor.g = nNewG;
}

void Text::SetLocation(int nNewX, int nNewY)
{
	m_nX = nNewX;
	m_nY = nNewY;
}

void Text::SetFont(std::string strNewFontName)
{
	m_strFontName = strNewFontName;
	UpdateTexture(true);
}