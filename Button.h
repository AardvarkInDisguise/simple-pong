#ifndef BUTTON_CLASS
#define BUTTON_CLASS

#include "standard_functions.h"
#include "Texture.h"

//button will only be in sate released if the mouse is still over the button after it was pressed
enum ButtonStates
{
	BUTTON_NULL,
	BUTTON_OVER,
	BUTTON_OUT,
	BUTTON_PRESSED,
	BUTTON_RELEASED
};

class Button
{
	private:
		//texture holding the button
		Texture textTexture;

		//attributes of the button (pos and dim)
		SDL_Rect box;
		//the button id used for logic
		int buttonID;

		SDL_Color textColor;

		std::string buttonText;

		int queueState;
		int buttonState;
		int previousState;

		bool isPressed;

		bool SetTexture();

	public:
		//initialize the variables with default values
		Button();
		//initialize the variables
		Button(int x, int y, int w, int h, std::string text, int id);
		//free the button surfaces
		~Button();
		//event handler, returns true if pressed
		void HandleEvents(SDL_Event event);
		void DoLogic();
		//show the button on the screen
		void Render();
		//returns the button's id
		int GetID();
		//has the button been pressed and released
		bool GetWasClicked();
		
		int GetState(){return buttonState;};
		int GetPreviousState(){return previousState;};

		void SetText(std::string text);
		std::string GetText(){return buttonText;};
};

#endif