#include "Button.h"

Button::Button()
{
	textColor.r = 255;
	textColor.g = 0;
	textColor.b = 0;

	//set pos and dim
	box.x = 50;
	box.y = 50;
	box.w = 210;
	box.h = 25;

	buttonText = "ERROR_DEFAULT_INFO";

	buttonID = -1;

	previousState = BUTTON_NULL;
	buttonState = BUTTON_NULL;
	queueState = BUTTON_NULL;

	isPressed = false;
}

Button::Button(int x, int y, int w, int h, std::string text, int id)
{
	textColor.r = 255;
	textColor.g = 255;
	textColor.b = 255;

	//set pos and dim
	box.x = x;
	box.y = y;
	box.w = w;
	box.h = h;

	buttonText = text;

	buttonID = id;

	buttonState = BUTTON_NULL;
	previousState = BUTTON_NULL;

	isPressed = false;

	SetTexture();
}

Button::~Button()
{
	textTexture.FreeTexture();
}

bool Button::SetTexture()
{
	TTF_Font *buttonFont = TTF_OpenFont("fonts/CaviarDreams-webfont.ttf", 32);
	assert(buttonFont != NULL);

	SDL_Surface* textSurface = NULL;
	textSurface = TTF_RenderText_Blended(buttonFont, buttonText.c_str(), textColor);
	if(textSurface == NULL)
		return false;

	if(textTexture.LoadTextureFromSDLSurface(textSurface) == false)
		return false;

	SDL_FreeSurface(textSurface);

	TTF_CloseFont(buttonFont);

	return true;
}

void Button::HandleEvents(SDL_Event event)
{
	//x, y of mouse
	int x = 0, y = 0;

	if(event.type == SDL_MOUSEMOTION)
	{
		x = event.motion.x;
		y = event.motion.y;

		//if the mouse is over the button
		if((x >= box.x) && (x <= box.x + box.w) && (y >= box.y) && (y <= box.y + box.h) && (buttonState != BUTTON_PRESSED))
		{
			queueState = BUTTON_OVER;
		}
		else if(!((x >= box.x) && (x <= box.x + box.w) && (y >= box.y) && (y <= box.y + box.h)))
		{
			queueState = BUTTON_OUT;
		}
	}
	else if(event.type == SDL_MOUSEBUTTONDOWN)
	{
		if(event.button.button == SDL_BUTTON_LEFT)
		{
			x = event.button.x;
			y = event.button.y;

			//if the mouse is over the button
			if((x >= box.x) && (x <= box.x + box.w) && (y >= box.y) && (y <= box.y + box.h))
			{
				queueState = BUTTON_PRESSED;
			}
		}
	}
	else if(event.type == SDL_MOUSEBUTTONUP)
	{
		if(event.button.button == SDL_BUTTON_LEFT)
		{
			x = event.button.x;
			y = event.button.y;

			//if the mouse is over the button
			if((x >= box.x) && (x <= box.x + box.w) && (y >= box.y) && (y <= box.y + box.h) && (buttonState == BUTTON_PRESSED))
			{
				queueState = BUTTON_RELEASED;
			}
		}
	}
}

void Button::DoLogic()
{
	//storage of the original button text color
	int r = 0, g = 0, b = 0;

	r = textColor.r;
	g = textColor.g;
	b = textColor.b;

	previousState = buttonState;

	if(buttonState != queueState)
	{

		buttonState = queueState;

		switch(buttonState)
		{
			case BUTTON_OVER:
				textTexture.FreeTexture();

				textColor.r = 125;
				textColor.g = 125;
				textColor.b = 125;

				SetTexture();

				textColor.r = r;
				textColor.g = g;
				textColor.b = b;
				break;

			case BUTTON_OUT:

				textTexture.FreeTexture();
				SetTexture();
				break;

			case BUTTON_PRESSED:
						
				textTexture.FreeTexture();

				textColor.r = 50;
				textColor.g = 50;
				textColor.b = 50;

				SetTexture();

				textColor.r = r;
				textColor.g = g;
				textColor.b = b;
				break;

			case BUTTON_RELEASED:
				textTexture.FreeTexture();
				SetTexture();
				isPressed = true;
				break;
		}
	}
}

void Button::Render()
{
	glLoadIdentity();


	glTranslatef(box.x, box.y, 0.0);

	glBegin(GL_QUADS);
		glColor3f(1.0, 1.0, 1.0);

		glVertex2f(0.f, 0.f);
		glVertex2f(box.w, 0.f);
		glVertex2f(box.w, box.h);
		glVertex2f(0.f, box.h);
	glEnd();


	glBegin(GL_QUADS);
		glColor3f(0.0, 0.0, 0.0);

		glVertex2f(1.f, 1.f);
		glVertex2f(box.w-1, 1.f);
		glVertex2f(box.w-1, box.h-1);
		glVertex2f(1.f, box.h-1);
	glEnd();

	textTexture.Render(box.w/2 - textTexture.GetTextureWidth()/2 + box.x, box.h/2 - textTexture.GetTextureHeight()/2 + box.y);
}

void Button::SetText(std::string text)
{
	buttonText = text;
	SetTexture();
}

int Button::GetID()
{
	return buttonID;
}

bool Button::GetWasClicked()
{
	return isPressed;
}