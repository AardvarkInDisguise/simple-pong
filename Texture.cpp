#include "Texture.h"

Texture::Texture()
{
	m_nTextureID = 0;
	m_nTextureWidth = 0;
	m_nTextureHeight = 0;
	m_pnPixels = 0;
}

Texture::~Texture()
{
	FreeTexture();
}

bool Texture::LoadTextureFromPixels32(GLvoid* pnPixels, GLuint nWidth, GLuint nHeight)
{
	//if we already have a texture loaded then free it
	FreeTexture();

	m_nTextureWidth = nWidth;
	m_nTextureHeight = nHeight;

	//generate texture id, not the actual texture
	glGenTextures(1, &m_nTextureID);

	//bind the texture id to a texture
	glBindTexture(GL_TEXTURE_2D, m_nTextureID);

	//generate the texture
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, nWidth, nHeight, 0, GL_BGRA, GL_UNSIGNED_BYTE, pnPixels);

	//set the texture parameters that control how the texture will be magnified and minified
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

	//we are finished working with the texture so unbind it
	glBindTexture(GL_TEXTURE_2D, NULL);

	//check for errors
	GLenum error = glGetError();
	if(error != GL_NO_ERROR)
	{
		printf("Error loading texture from %p pixels! %s\n", pnPixels, gluErrorString(error));
		return false;
	}

	return true;
}

bool Texture::LoadTextureFromFile(std::string strFile)
{
	//load the image into an SDL surface
	SDL_Surface *srfTemp = TS::LoadImageA(strFile.c_str());
	if(srfTemp == NULL)
	{
		return false;
	}
	//send the pixels from the surface to be applied to the texture
	if(!LoadTextureFromPixels32(srfTemp->pixels, srfTemp->w, srfTemp->h))
	{
		SDL_FreeSurface(srfTemp);
		return false;
	}
	SDL_FreeSurface(srfTemp);
	return true;
}

bool Texture::LoadTextureFromSDLSurface(SDL_Surface* srf)
{
	if(srf == NULL)
	{
		return false;
	}
	//send the pixels from the surface to be applied to the texture
	if(!LoadTextureFromPixels32(srf->pixels, srf->w, srf->h))
	{
		return false;
	}
	return true;
}

void Texture::FreeTexture()
{
	//if the texture isn't NULL then delete it
	if(m_nTextureID != 0)
	{
		glDeleteTextures(1, &m_nTextureID);
		m_nTextureID = 0;
	}
	if(m_pnPixels != NULL)
	{
		delete[] m_pnPixels;
		m_pnPixels = NULL;
	}

	m_nTextureWidth = 0;
	m_nTextureHeight = 0;
}

void Texture::Render(GLfloat fX, GLfloat fY)
{
	Render(fX, fY, NULL, false);
}

void Texture::Render(GLfloat fX, GLfloat fY, bool bOverrideColor)
{
	Render(fX, fY, NULL, bOverrideColor);
}

void Texture::Render(GLfloat fX, GLfloat fY, FRect* recClip, bool bOverrideColor)
{
	//check that the texture exists
	if(m_nTextureID != 0)
	{
		//reset the vertex color
		if(!bOverrideColor)
			glColor3f(1.f, 1.f, 1.f);

		GLfloat fTexTop    = 0.f;
		GLfloat fTexBottom = 1.f;
		GLfloat fTexLeft   = 0.f;
		GLfloat fTexRight  = 1.f;

		GLfloat fQuadWidth = m_nTextureWidth;
		GLfloat fQuadHeight = m_nTextureHeight;

		if(recClip != NULL)
		{
			fTexTop	   = recClip->y/m_nTextureHeight;
			fTexBottom = (recClip->y + recClip->h)/m_nTextureHeight;
			fTexLeft   = recClip->x/m_nTextureWidth;
			fTexRight  = (recClip->x + recClip->w)/m_nTextureWidth;

			fQuadWidth = recClip->w;
			fQuadHeight = recClip->h;
		}

		//remove any previous transformations
		glLoadIdentity();

		//move to the render point
		glTranslatef(fX, fY, 0.0);

		//bind the texture id
		glBindTexture(GL_TEXTURE_2D, m_nTextureID);

		glBegin(GL_QUADS);
			glTexCoord2f(fTexLeft, fTexTop);     glVertex2f(	   0.f, 0.f);
			glTexCoord2f(fTexRight, fTexTop);    glVertex2f(fQuadWidth,	0.f);
			glTexCoord2f(fTexRight, fTexBottom); glVertex2f(fQuadWidth, fQuadHeight);
			glTexCoord2f(fTexLeft, fTexBottom);  glVertex2f(	   0.f, fQuadHeight);
		glEnd();

		glBindTexture(GL_TEXTURE_2D, NULL);
	}
}

//lock the texture so it can be edited
bool Texture::Lock()
{
	//if m_pnPixles is not NULL the the texture is already locked
	if(m_pnPixels == NULL && m_nTextureID != 0)
	{
		//allocate memory for pixel data
		GLuint nSize = m_nTextureWidth * m_nTextureHeight;
		m_pnPixels = new GLuint [nSize];

		//bind the texture so we can access it
		glBindTexture(GL_TEXTURE_2D, m_nTextureID);

			//set m_pnPixels to point to the pixel data
			glGetTexImage(GL_TEXTURE_2D, 0, GL_RGBA, GL_UNSIGNED_BYTE, m_pnPixels);

		//we have finished accessing the texture
		glBindTexture(GL_TEXTURE_2D, NULL);

		return true;
	}
	return false;
}
//unlock the texture and apply edits
bool Texture::UnLock()
{
	if(m_pnPixels != NULL && m_nTextureID != 0)
	{
		//bind the texture for access
		glBindTexture(GL_TEXTURE_2D, m_nTextureID);
			//update texture with new pixel info
			glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, m_nTextureWidth, m_nTextureHeight, GL_RGBA, GL_UNSIGNED_BYTE, m_pnPixels);
		//unbind texture
		glBindTexture(GL_TEXTURE_2D, NULL);

		delete[] m_pnPixels;
		m_pnPixels = NULL;

		return true;
	}
	return false;
}

GLuint* Texture::GetPixelData32()
{
	return m_pnPixels;
}

GLuint Texture::GetPixel32(GLuint x, GLuint y)
{
	return m_pnPixels[y * m_nTextureWidth + x];
}

void Texture::SetPixel32(GLuint x, GLuint y, GLuint nPixel)
{
	m_pnPixels[y * m_nTextureWidth + x] = nPixel;
}