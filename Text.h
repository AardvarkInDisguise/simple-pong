#ifndef TEXT_H
#define TEXT_H

#include "standard_functions.h"
#include "Texture.h"

class Text
{
	private:
		Texture m_texTextTexture;

		std::string m_strFontName;
		std::string m_strText;

		SDL_Color m_colTextColor;
		
		int m_nFontSize;
		int m_nX;
		int m_nY;
		int m_nW;
		int m_nH;

		TTF_Font* m_fntFont;

		void UpdateTexture(bool bReloadFont);

	public:
		Text(std::string strText, std::string strFont, int nSize, short int nR, short int nG, short int nB, int nX, int nY);
		Text(std::string strText, std::string strFont, int nSize, SDL_Color colColor, int nX, int nY);
		~Text();

		void Render();

		void SetText(std::string strNewText);
		void SetFontSize(int nNewSize);
		void SetColor(short int nNewR, short int nNewG, short int nNewB);
		void SetColor(SDL_Color colNewColor);
		void SetLocation(int nNewX, int nNewY);
		void SetFont(std::string strNewFontName);

		std::string GetText()	 {return m_strText;};
		std::string GetFontName(){return m_strFontName;};
		SDL_Color   GetColor()	 {return m_colTextColor;};
		int			GetFontSize(){return m_nFontSize;};
		SDL_Rect	GetDim()	 {SDL_Rect recDim; recDim.h = m_nH; recDim.w = m_nW; return recDim;};
		SDL_Rect	GetPos()	 {SDL_Rect recPos; recPos.x = m_nX; recPos.y = m_nY; return recPos;};
};

#endif