#ifndef TIMER_CLASS
#define TIMER_CLASS

#include "standard_functions.h"

class Timer
{
	private:
		//clock time when the timer started
		int startTicks;
		//timer time when the timer was paused
		int pausedTicks;

		//is the timer paused, is only true if bStarted is true (explained below)
		bool bPaused;

		//has the timer started ,both this and bPaused can be true the difference is bStarted is true
		//if the timer has been started bPaused is true if the timer has been started but isn't counting right now,
		//therefore bPaused can only be true if bStarted is true
		bool bStarted;

	public:
		//initialize the variables
		Timer();

		//starts the timer, if the timer is already running then it essentially resets it
		void Start();
		//stop the timer and reset values
		void Stop();
		//pause the timer
		void Pause();
		//unpause the timer, start counting up again from whatever number the timer reached
		void UnPause();

		//get the timers current time, if the timer isn't running returns -1
		int GetTime();
		//set the timer's current time
		void SetTime(int time);


		//---We use these so we know that the start and stop booleans can't be changed outside of the class therefore keeping their values predictable---
		//get if the timer has been started
		bool IsStarted();
		//get if the timer is currently paused
		bool IsPaused();
};

#endif