#include "GameStatePlaying.h"

GameStatePlaying::GameStatePlaying()
	:m_txtPlOneScore("0", "fonts/CaviarDreams-webfont.ttf", 28, 255, 255, 255, 30, 10),
	m_txtPlTwoScore("0", "fonts/CaviarDreams-webfont.ttf", 28, 255, 255, 255, 30, 10)
{
	std::cout << "GameState = Playing" << std::endl;

	stateID = GAMESTATE_PLAY;
	nextStateID = GAMESTATE_NULL;

	m_vecEnts.push_back(new EntityPaddle(10, 200, 10, 100/2, &m_vecEnts, true));
	m_vecEnts.push_back(new EntityPaddle(SCREEN_WIDTH - 20, 200, 10, 100/2, &m_vecEnts, false));
	m_vecEnts.push_back(new EntityBall(350, 300, 15, &m_vecEnts, 150, m_anPlayerScores));

	m_anPlayerScores[0] = 0;
	m_anPlayerScores[1] = 0;

	//set the player's score to the appropriate position
	m_txtPlOneScore.SetLocation(SCREEN_WIDTH - (m_txtPlOneScore.GetDim().w + 30), m_txtPlOneScore.GetPos().y);
}

GameStatePlaying::~GameStatePlaying()
{
	for(std::vector<Entity*>::iterator iii = m_vecEnts.begin(); iii != m_vecEnts.end(); iii++)
	{
		delete((*iii));
		std::cout << "ENT DELETE" << std::endl;
		m_vecEnts.erase(iii--);
	}
}

void GameStatePlaying::HandleEvents(SDL_Event &event)
{
	for(std::vector<Entity*>::iterator iii = m_vecEnts.begin(); iii != m_vecEnts.end(); iii++)
	{
		(*iii)->HandleEvents(event);
	}

	if(event.type == SDL_KEYDOWN)
	{
		if(event.key.keysym.sym == SDLK_ESCAPE)
		{
			nextStateID = GAMESTATE_TITLE;
		}
	}
}

void GameStatePlaying::Dologic()
{
	bool bNeedsNewBall = false;
	//delete entities marked for deletion
	for(std::vector<Entity*>::iterator iii = m_vecEnts.begin(); iii != m_vecEnts.end(); iii++)
	{
		if((*iii)->GetShouldDelete())
		{
			if(typeid(*(*iii)) == typeid(EntityBall))
				bNeedsNewBall = true;
			delete(*iii);
			std::cout << "ENT DELETE" << std::endl;
			m_vecEnts.erase(iii--);
		}
	}
	//if the ball was deleted then create a new one and update score text
	if(bNeedsNewBall)
	{
		m_vecEnts.push_back(new EntityBall(350, 300, 15, &m_vecEnts, 150, m_anPlayerScores));

		m_txtPlOneScore.SetText(std::to_string((long double)m_anPlayerScores[0]));
		m_txtPlTwoScore.SetText(std::to_string((long double)m_anPlayerScores[1]));
		//set the player's score to the appropriate position
		m_txtPlOneScore.SetLocation(SCREEN_WIDTH - (m_txtPlOneScore.GetDim().w + 30), m_txtPlOneScore.GetPos().y);
	}

	//do movement, collision and logic for each entity
	for(std::vector<Entity*>::iterator iii = m_vecEnts.begin(); iii != m_vecEnts.end(); iii++)
	{
		(*iii)->DoMovement();
	}
	for(std::vector<Entity*>::iterator iii = m_vecEnts.begin(); iii != m_vecEnts.end(); iii++)
	{
		(*iii)->DoCollision();
	}
	for(std::vector<Entity*>::iterator iii = m_vecEnts.begin(); iii != m_vecEnts.end(); iii++)
	{
		(*iii)->DoLogic();
	}
}

void GameStatePlaying::DoRender()
{
	//render all entities
	for(std::vector<Entity*>::iterator iii = m_vecEnts.begin(); iii != m_vecEnts.end(); iii++)
	{
		(*iii)->Render();
	}

	m_txtPlOneScore.Render();
	m_txtPlTwoScore.Render();
}