#ifndef GAMESTATEPLAYING_H
#define GAMESTATEPLAYING_H

#include "GameState.h"
#include "EntityPaddle.h"
#include "EntityBall.h"
#include "Text.h"

class GameStatePlaying : public GameState
{
	private:
		std::vector<Entity*> m_vecEnts;
		int m_anPlayerScores[2];

		Text m_txtPlOneScore;
		Text m_txtPlTwoScore;

	public:
		GameStatePlaying();
		virtual void HandleEvents(SDL_Event &event);
		//logic includes movement and collision
		virtual void Dologic();
		virtual void DoRender();
		virtual int GetStateID(){return stateID;};
		virtual int GetNextStateID(){return nextStateID;};
		virtual ~GameStatePlaying();
};

#endif