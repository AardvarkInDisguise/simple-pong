#ifndef ENTITYBALL_H
#define ENTITYBALL_H

#include "Entity.h"
#include "EntityPaddle.h"

class EntityBall : public Entity
{
	private:
		float m_fDir;
		std::vector<Entity*> *m_vecEnts;

		int m_nSpeed;

		//if true then hitting the edge (left or right side) will mark the ball for delete
		bool m_bEdgeKills;
		//if true then the balls speed increases when it hits a paddle
		bool m_bDynamicSpeed;

		int *m_anPlayerScores;

		int m_nLastPlayerHit;

		void SetDir(float fDir);
		void SetSpeed(int nSpeed);
		int  GetCollisionSide(FRect frecOtherEnt);
		//flips the X velocity and updates the direction variable
		void FlipXVel();
		//flips the Y velocity and updates the direction variable
		void FlipYVel();

	public:
		EntityBall(int x, int y, int diameter, std::vector<Entity*> *entVec, int nInitSpeed, int *anPlayerScores = NULL, bool bDynSpd = true, bool bEdgeKills = true);
		virtual void HandleEvents(SDL_Event event);
		virtual void DoMovement();
		virtual void DoCollision();
		virtual void DoLogic();
		virtual void Render();
		virtual ~EntityBall();
};

#endif