#ifndef COLLISION_H
#define COLLISION_H

#include "SDL.h"
#include "FRect.h"

enum CollisionTypes
{
	COLLISION_SCREENRANGE,
	COLLISION_SCREENDOMAIN,
	COLLISION_ENTITY
};

struct Collision
{
	int type;
	FRect BBox;
	int CircR;
	std::string typeName;
};

#endif