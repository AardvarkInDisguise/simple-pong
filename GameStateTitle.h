#ifndef GAMESTATETITLE_H
#define GAMESTATETITLE_H

#include "GameState.h"
#include "EntityPaddle.h"
#include "EntityBall.h"

class GameStateTitle : public GameState
{
	private:
		Button m_btnPlay;
		Button m_btnQuit;

		std::vector<Entity*> m_vecEnts;

		Texture m_texTitle;

	public:
		GameStateTitle();
		virtual void HandleEvents(SDL_Event &event);
		//logic includes movement and collision
		virtual void Dologic();
		virtual void DoRender();
		virtual int GetStateID(){return stateID;};
		virtual int GetNextStateID(){return nextStateID;};
		virtual ~GameStateTitle();
};

#endif