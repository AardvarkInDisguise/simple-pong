#ifndef GAMESTATE_H
#define GAMESTATE_H

#include "standard_functions.h"
#include "Texture.h"
#include "timer.h"
#include "Button.h"
#include "FRect.h"
#include "Entity.h"

enum GameStates
{
	GAMESTATE_EXIT = -1,
	GAMESTATE_NULL,
	GAMESTATE_TITLE,
	GAMESTATE_PLAY
};

class GameState
{
	protected:
		//id of the current state
		int stateID;
		//id of the state to change to after this loop, all states are greater than 0, if next state equals 0 then no change, if next state equals -1 then user wants to quit
		int nextStateID;

	public:
		virtual void HandleEvents(SDL_Event &event) = 0;
		//logic includes movement and collision
		virtual void Dologic() = 0;
		virtual void DoRender() = 0;
		virtual int GetStateID() = 0;
		virtual int GetNextStateID() = 0;
		virtual ~GameState(){};
};

#endif